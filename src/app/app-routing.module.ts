import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClinicalRecordComponent } from './components/home/clinical-record/clinical-record.component';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { PatientComponent } from './components/home/patient/patient.component';
import { InfoBackgroundComponent } from './components/home/clinical-record/info-background/info-background.component';
import { PediatricsComponent } from './components/home/clinical-record/pediatrics/pediatrics.component';
import { OsteopathicFollowUpComponent } from './components/home/clinical-record/osteopathic-follow-up/osteopathic-follow-up.component';
import { ConsultationsComponent } from './components/home/clinical-record/consultations/consultations.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'home', component: HomeComponent, children: [
      {
        path: 'clinical-record', component: ClinicalRecordComponent, children: [
          
          { path: 'info-background', component: InfoBackgroundComponent },
          { path: 'pediatrics', component: PediatricsComponent },
          { path: 'osteopathic-follow-up', component: OsteopathicFollowUpComponent },
          { path: 'consultations', component: ConsultationsComponent },

        ]
      },
      { path: 'patient', component: PatientComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
