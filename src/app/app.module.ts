import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';

import { HomeComponent } from './components/home/home.component';
import { ClinicalRecordComponent } from './components/home/clinical-record/clinical-record.component';
import { PatientComponent } from './components/home/patient/patient.component';
import { InfoBackgroundComponent } from './components/home/clinical-record/info-background/info-background.component';
import { PediatricsComponent } from './components/home/clinical-record/pediatrics/pediatrics.component';
import { OsteopathicFollowUpComponent } from './components/home/clinical-record/osteopathic-follow-up/osteopathic-follow-up.component';
import { ConsultationsComponent } from './components/home/clinical-record/consultations/consultations.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ClinicalRecordComponent,
    PatientComponent,
    InfoBackgroundComponent,
    PediatricsComponent,
    OsteopathicFollowUpComponent,
    ConsultationsComponent
  ],
  imports: [
    // Default
    BrowserModule,
    AppRoutingModule,
    // Used by login
    FormsModule,
    // Used by login
    ReactiveFormsModule,
    // Used by login
    BrowserAnimationsModule,
    // Used by login
    MatFormFieldModule,
    // Used by login & home & clinical-record
    MatButtonModule,
    // Used by login
    MatInputModule,
    // Used by login & home
    MatIconModule,
    // Used by login
    MatCardModule,
    // Used by info-background
    MatDividerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
