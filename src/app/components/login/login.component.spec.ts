import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(waitForAsync(() => {

    // Sets up the testing environment by configuring the testing module.
    TestBed.configureTestingModule({
      // It imports the necessary modules
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        MatCardModule

      ],
      declarations: [LoginComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  }));

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form should be invalid when empty', () => {
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('email field validity', () => {
    let email = component.loginForm.controls['email'];
    expect(email.valid).toBeFalsy();

    email.setValue('ctom.99@outlook.fr');
    expect(email.valid).toBeTruthy();
  });

  it('password field validity', () => {
    let password = component.loginForm.controls['password'];
    expect(password.valid).toBeFalsy();

    password.setValue('Test123!');
    expect(password.valid).toBeTruthy();
  });

  it('form should be valid when all fields are filled', () => {
    component.loginForm.controls['email'].setValue('ctom.99@outlook.fr');
    component.loginForm.controls['password'].setValue('Test123!');
    expect(component.loginForm.valid).toBeTruthy();
  });

  it('should navigate to home page when form is submitted and valid', () => {
    component.loginForm.controls['email'].setValue('ctom.99@outlook.fr');
    component.loginForm.controls['password'].setValue('Test123!');
    spyOn(component.router, 'navigate');
    component.onSubmit();
    expect(component.router.navigate).toHaveBeenCalledWith(['/home/patient']);
  });

  it('should show error message when form is submitted and invalid', () => {
    component.loginForm.controls['email'].setValue('');
    component.loginForm.controls['password'].setValue('');
    component.onSubmit();
    expect(component.showError).toBeTruthy();
  });

  it('should toggle password visibility', () => {
    component.showPassword = false;
    component.togglePasswordVisibility();
    expect(component.showPassword).toBeTruthy();

    component.togglePasswordVisibility();
    expect(component.showPassword).toBeFalsy();
  });

});
