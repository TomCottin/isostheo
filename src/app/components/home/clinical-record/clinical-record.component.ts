import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-clinical-record',
  templateUrl: './clinical-record.component.html',
  styleUrls: ['./clinical-record.component.scss']
})
export class ClinicalRecordComponent {

  constructor(private router: Router) {}

  displayInfoBackground() {
    this.router.navigate(['/home/clinical-record/info-background']);
  }

  displayPediatrics() {
    this.router.navigate(['/home/clinical-record/pediatrics']);
  }

  displayOsteopathicFollowUp() {
    this.router.navigate(['home/clinical-record/osteopathic-follow-up']);
  }

  displayConsultations() {
    this.router.navigate(['home/clinical-record/consultations']);
  }
}
