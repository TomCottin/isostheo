import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OsteopathicFollowUpComponent } from './osteopathic-follow-up.component';

describe('OsteopathicFollowUpComponent', () => {
  let component: OsteopathicFollowUpComponent;
  let fixture: ComponentFixture<OsteopathicFollowUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OsteopathicFollowUpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OsteopathicFollowUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
