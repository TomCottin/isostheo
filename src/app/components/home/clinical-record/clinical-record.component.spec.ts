import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { ClinicalRecordComponent } from './clinical-record.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('ClinicalRecordComponent', () => {
  let component: ClinicalRecordComponent;
  let fixture: ComponentFixture<ClinicalRecordComponent>;
  let router: Router;

  beforeEach(waitForAsync((() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ ClinicalRecordComponent ]
    })
    .compileComponents();
  })));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicalRecordComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to info background', () => {
    spyOn(router, 'navigate');
    component.displayInfoBackground();
    expect(router.navigate).toHaveBeenCalledWith(['/home/clinical-record/info-background']);
  });

  it('should navigate to pediatrics', () => {
    spyOn(router, 'navigate');
    component.displayPediatrics();
    expect(router.navigate).toHaveBeenCalledWith(['/home/clinical-record/pediatrics']);
  });

  it('should navigate to osteopathic follow up', () => {
    spyOn(router, 'navigate');
    component.displayOsteopathicFollowUp();
    expect(router.navigate).toHaveBeenCalledWith(['home/clinical-record/osteopathic-follow-up']);
  });

  it('should navigate to consultations', () => {
    spyOn(router, 'navigate');
    component.displayConsultations();
    expect(router.navigate).toHaveBeenCalledWith(['home/clinical-record/consultations']);
  });
});
