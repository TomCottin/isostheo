import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { InfoBackgroundComponent } from './info-background.component';

describe('InfoBackgroundComponent', () => {
  let component: InfoBackgroundComponent;
  let fixture: ComponentFixture<InfoBackgroundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDividerModule
      ],
      declarations: [ InfoBackgroundComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InfoBackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a form with 11 controls', () => {
    expect(Object.keys(component.infoBackgroundForm.controls).length).toEqual(15);
  });

  it('should have all controls as disabled', () => {
    Object.keys(component.infoBackgroundForm.controls).forEach((controlName) => {
      expect(component.infoBackgroundForm.get(controlName)?.disabled).toBeTruthy();
    });
  });

  it('should have initial values for all controls', () => {
    expect(component.infoBackgroundForm.controls.name.value).toEqual('Cottin');
    expect(component.infoBackgroundForm.controls.firstname.value).toEqual('Tom');
    expect(component.infoBackgroundForm.controls.birthdate.value).toEqual('31/01/1999');
    expect(component.infoBackgroundForm.controls.maritalStatus.value).toEqual('Célibataire');
    expect(component.infoBackgroundForm.controls.profession.value).toEqual('Développeur');
    expect(component.infoBackgroundForm.controls.address.value).toEqual('33 avenue Mathieu Misery');
    expect(component.infoBackgroundForm.controls.zipCode.value).toEqual('69160');
    expect(component.infoBackgroundForm.controls.town.value).toEqual('Tassin-la-Demi-Lune');
    expect(component.infoBackgroundForm.controls.email.value).toEqual('ctom.99@outlook.fr');
    expect(component.infoBackgroundForm.controls.phone.value).toEqual('0603350492');
    expect(component.infoBackgroundForm.controls.sport.value).toEqual('Handball');
    expect(component.infoBackgroundForm.controls.socialSecurityNumber.value).toEqual('1 99 69 xxxx');
    expect(component.infoBackgroundForm.controls.doctor.value).toEqual('Aucun');
    expect(component.infoBackgroundForm.controls.osteopath.value).toEqual('Pierre Bourbon');
    expect(component.infoBackgroundForm.controls.clinicDiscovery.value).toEqual('Amis');
  });
});
