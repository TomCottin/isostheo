import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-info-background',
  templateUrl: './info-background.component.html',
  styleUrls: ['./info-background.component.scss']
})
export class InfoBackgroundComponent {

  infoBackgroundForm = new FormGroup({
    name: new FormControl({value: 'Cottin', disabled: true}),
    firstname: new FormControl({value: 'Tom', disabled: true}),
    birthdate: new FormControl({value: '31/01/1999', disabled: true}),
    maritalStatus: new FormControl({value: 'Célibataire', disabled: true}),
    profession: new FormControl({value: 'Développeur', disabled: true}),
    address: new FormControl({value: '33 avenue Mathieu Misery', disabled: true}),
    zipCode: new FormControl({value: '69160', disabled: true}),
    town: new FormControl({value: 'Tassin-la-Demi-Lune', disabled: true}),
    email: new FormControl({value: 'ctom.99@outlook.fr', disabled: true}),
    phone: new FormControl({value: '0603350492', disabled: true}),
    sport: new FormControl({value: 'Handball', disabled: true}),
    socialSecurityNumber: new FormControl({value: '1 99 69 xxxx', disabled: true}),
    doctor: new FormControl({value: 'Aucun', disabled: true}),
    osteopath: new FormControl({value: 'Pierre Bourbon', disabled: true}),
    clinicDiscovery: new FormControl({value: 'Amis', disabled: true}),
  });
}
