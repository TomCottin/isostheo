import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let routerSpy: jasmine.SpyObj<Router>;

  beforeEach(() => {
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatIconModule
      ],
      declarations: [ HomeComponent ],
      providers: [
        { provide: Router, useValue: routerSpy }
      ]
    });

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to /home/clinical-record/info-background on displayClinicalRecord()', () => {
    component.displayClinicalRecord();
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/home/clinical-record/info-background']);
  });

  it('should navigate to /home/patient on displayPatient()', () => {
    component.displayPatient();
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/home/patient']);
  });

  it('should navigate to / on logOut()', () => {
    component.logOut();
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/']);
  });
  
});
