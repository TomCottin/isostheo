import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  isLoggedIn: boolean = true;

  constructor(private router: Router) {}

  displayClinicalRecord() {
    this.router.navigate(['/home/clinical-record/info-background']);
  }

  displayPatient() {
    this.router.navigate(['/home/patient']);
  }

  logOut() {
    // perform log out functionality, for example clear the local storage
    // ...

    this.router.navigate(['/']);
  }

}
